<?php 

class Ship {

	private $name;

	private $weaponPower = 0;

	private $jediFactor = 0;

	private $strength = 0;

	private $underRepair;



	public function __construct( $name ) {
		$this->name = $name;
		$this->underRepair = mt_rand( 1, 100 ) < 30;
	}

	public function isFunctional() {
		return !$this->underRepair;
	} 

	public function sayHello() {
		echo 'HELLO!';
	}

	/**
     * Gets the value of name.
     *
     * @return mixed
     */
	public function getName() {
		return $this->name;
	}

	/**
     * Sets the value of name.
     *
     * @param mixed $name the name
     *
     * @return self
     */
	public function setName($name)
	{
		$this->name = $name;

		return $this;
	}

	/**
     * Sets the value of strength.
     *
     * @param mixed $strength the strength
     *
     * @return self
     */
	public function setStrength( $strength ) {

		if ( !is_numeric( $strength ) ) {
			throw new Exception( 'Invalid strength passed ' . $strength );
		}

		$this->strength = $strength;

		return $this;
	}

	/**
     * Gets the value of strength.
     *
     * @return mixed
     */
	public function getStrength() {
		return $this->strength;
	}

    /**
     * Gets the value of weaponPower.
     *
     * @return mixed
     */
    public function getWeaponPower()
    {
    	return $this->weaponPower;
    }

    /**
     * Sets the value of weaponPower.
     *
     * @param mixed $weaponPower the weapon power
     *
     * @return self
     */
    public function setWeaponPower($weaponPower)
    {
    	$this->weaponPower = $weaponPower;

    	return $this;
    }

    /**
     * Gets the value of jediFactor.
     *
     * @return mixed
     */
    public function getJediFactor()
    {
    	return $this->jediFactor;
    }

    /**
     * Sets the value of jediFactor.
     *
     * @param mixed $jediFactor the jedi factor
     *
     * @return self
     */
    public function setJediFactor($jediFactor)
    {
    	$this->jediFactor = $jediFactor;

    	return $this;
    }

    public function getNameAndSpecs( $useShortFormat = false ) {

    	if ( $useShortFormat ) {
    		return sprintf(
    			'%s: %s/%s/%s',
    			$this->name,
    			$this->weaponPower,
    			$this->jediFactor,
    			$this->strength 
    			);
    	} else {
    		return sprintf(
    			'%s: w:%s, j:%s, s:%s',
    			$this->name,
    			$this->weaponPower,
    			$this->jediFactor,
    			$this->strength 
    			);
    	}

    }

    public function doesGivenShipHaveMoreStrength( $givenShip ) {
    	return $givenShip->strength > $this->strength;
    }

}